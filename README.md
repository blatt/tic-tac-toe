# Tic Tac Toe

> A vue.js tic tac toe game

Play Tic Tac Toe in your browser (Two Player).

## Build status

![pipeline status](https://gitlab.com/blatt/tic-tac-toe/badges/master/pipeline.svg) ![coverage report](https://gitlab.com/blatt/tic-tac-toe/badges/master/coverage.svg)

## Screenshots

todo

## Tech/framework used

Built with

* [Nodejs](https://nodejs.org/)
* [Vuejs](https://vuejs.org/)

## Installation

``` bash
# install dependencies
npm install
```

## How to use

``` bash
# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

## Tests

todo

## Contribute

All contributions are always welcome.

## Credits

The tic tac toe game based on tutorial code from [Hammad Ahmed](https://github.com/shammadahmed).

## License  [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/blatt/tic-tac-toe/raw/master/LICENSE)

© Distributed under the terms of the [MIT License](https://gitlab.com/blatt/tic-tac-toe/raw/master/LICENSE).
